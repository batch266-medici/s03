package a1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class a1 {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed: ");


        try {
            int integer = userInput.nextInt();
            int factorial= 1;

            if (integer == 0) {
                System.out.println("Please input a number greater than 0.");
            } else if (integer < 0) {
                System.out.println("Please input a positive number.");
            } else {
                for (int i = 1; i <= integer; i++) {
                    factorial = factorial*i;
                }
                System.out.println("The factorial of " + integer + " is " + factorial);
            }
        } catch (InputMismatchException e) {
            System.out.println("Only numbers are accepted for this system.");
        }
    }
}
